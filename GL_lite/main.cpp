#include "GLlite.h"

int main(int argc, char** argv)
{
	initializeGLFW(1920, 1080, "GLlite", true);

	initializeOpenGL();

	Texture texture("window.DDS");
	Sprite s(100.0f, 100.0f, 1080.0f, 1080.0f, 15.0f, COLOR_WHITE, &texture, TexCoords {0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f});

	bool running = true;
	while (running) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
			running = false;

		s.render();
		s.rotate(0.1f);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}