#include "GLlite.h"

#define FOURCC_DXT1 0x31545844
#define FOURCC_DXT3 0x33545844
#define FOURCC_DXT5 0x35545844

const GLColor COLOR_RED = { 1.0f, 0.0f, 0.0f, 1.0f };
const GLColor COLOR_GREEN = { 0.0f, 1.0f, 0.0f, 1.0f };
const GLColor COLOR_BLUE = { 0.0f, 0.0f, 1.0f, 1.0f };
const GLColor COLOR_WHITE = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLColor COLOR_BLACK = { 0.0f, 0.0f, 0.0f, 1.0f };

// Window
GLFWwindow* window;
GLuint shaderProgram;
GLuint transformLoc;
int windowWidth, windowHeight;
double windowHalfWidth, windowHalfHeight;
double mouseX = 0, mouseY = 0;
double scrollX = 0, scrollY = 0;

void
GLliteCursorPosCallback(GLFWwindow* window, double xpos, double ypos)
{
	mouseX = xpos;
	mouseY = ypos;
}

void
GLliteScrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
	scrollX += xoffset;
	scrollY += yoffset;
}

void
initializeGLFW(const int& width, const int& height, const std::string& name, const bool& fullScreen)
{
	if (!glfwInit())
		throw std::runtime_error("GLFW failed to initialize");

	glfwWindowHint(GLFW_SAMPLES, 8);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

	fullScreen ? window = glfwCreateWindow(width, height, "GL lite", glfwGetPrimaryMonitor(), NULL) : window = glfwCreateWindow(width, height, "GL lite", NULL, NULL);
	if (!window) {
		glfwTerminate();
		throw std::runtime_error("The GLFW window failed to intitialize");
	}

	windowWidth = width;
	windowHeight = height;
	windowHalfWidth = (double)width / 2.0;
	windowHalfHeight = (double)height / 2.0;
	glfwSetCursorPosCallback(window, GLliteCursorPosCallback);
	glfwSetScrollCallback(window, GLliteScrollCallback);
}

GLint vertexColorLocation;

void
initializeOpenGL()
{
	glfwMakeContextCurrent(window);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_MULTISAMPLE);

	glewExperimental = GL_TRUE;
	glewInit();
	glViewport(0, 0, windowWidth, windowHeight);

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	std::string vertexSource;
	std::ifstream VertexShaderStream("vertex.vs", std::ios::in);
	if (VertexShaderStream.is_open()) {
		std::string Line = "";
		while (getline(VertexShaderStream, Line))
			vertexSource += "\n" + Line;
		VertexShaderStream.close();
	}
	else {
		throw std::invalid_argument("vertex.vs not found");
	}
	const char* vertexSourcePtr = vertexSource.c_str();

	std::string fragmentSource;
	std::ifstream FragmentShaderStream("fragment.fs", std::ios::in);
	if (FragmentShaderStream.is_open()) {
		std::string Line = "";
		while (getline(FragmentShaderStream, Line))
			fragmentSource += "\n" + Line;
		FragmentShaderStream.close();
	}
	else {
		throw std::invalid_argument("fragment.fs not found");
	}
	const char* fragmentSourcePtr = fragmentSource.c_str();

	glShaderSource(vertexShader, 1, &vertexSourcePtr, NULL);
	glCompileShader(vertexShader);
	GLint success;
	GLchar infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		throw std::runtime_error("Vertex Shader failed to compile!");
	}

	glShaderSource(fragmentShader, 1, &fragmentSourcePtr, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
		throw std::logic_error("Fragment Shader failed to compile!");
	}

	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	
	glUseProgram(shaderProgram);

	transformLoc = glGetUniformLocation(shaderProgram, "transform");
}

bool
isMouseButtonDown(const int& button)
{
	if (glfwGetMouseButton(window, button) == GLFW_PRESS)
		return true;
	else
		return false;
}

double
getMouseX()
{
	return mouseX;
}

double
getMouseY()
{
	return mouseY;
}

double
getScrollX()
{
	double temp = scrollX;
	scrollX = 0;
	return temp;
}

double
getScrollY()
{
	double temp = scrollY;
	scrollY = 0;
	return temp;
}

int
getWindowWidth()
{
	return windowWidth;
}

int
getWindowHeight()
{
	return windowHeight;
}

// Texture (ONLY loads .dds image files)
Texture::Texture(const char* imgPath)
{
	unsigned char header[124];
	FILE* fp;

	fopen_s(&fp, imgPath, "rb");
	if (fp == NULL)
		throw std::runtime_error("Unable to open texture file");

	char filecode[4];
	fread(filecode, 1, 4, fp);
	if (strncmp(filecode, "DDS ", 4) != 0) {
		fclose(fp);
		throw std::runtime_error("Texture file is not of type DDS");
	}

	fread(&header, 124, 1, fp);

	height_ = *(unsigned int*)&(header[8]);
	width_ = *(unsigned int*)&(header[12]);
	linearSize_ = *(unsigned int*)&(header[16]);
	mipMapCount_ = *(unsigned int*)&(header[24]);
	fourCC_ = *(unsigned int*)&(header[80]);

	unsigned char* buffer;
	unsigned int bufsize;
	bufsize = mipMapCount_ > 1 ? linearSize_ * 2 : linearSize_;
	buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char));
	fread(buffer, 1, bufsize, fp);
	fclose(fp);

	unsigned int comonents = (fourCC_ == FOURCC_DXT1) ? 3 : 4;
	unsigned int format;
	switch (fourCC_) {
	case FOURCC_DXT1:
		format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
		break;
	case FOURCC_DXT3:
		format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
		break;
	case FOURCC_DXT5:
		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
		break;
	default:
		free(buffer);
		throw std::runtime_error("Unsupported texture compression");
	}

	glGenTextures(1, &textureID_);
	glBindTexture(GL_TEXTURE_2D, textureID_);

	unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
	unsigned int offset = 0;

	unsigned int width = width_;
	unsigned int height = height_;
	for (unsigned int level = 0; level < mipMapCount_ && (width || height); ++level) {
		unsigned int size = ((width + 3) / 4) * ((height + 3) / 4) * blockSize;
		glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height, 0, size, buffer + offset);

		offset += size;
		width /= 2;
		height /= 2;

		if (width < 1) width = 1;
		if (height < 1) height = 1;
	}
	
	free(buffer);
}

void
Texture::bind()
{
	glBindTexture(GL_TEXTURE_2D, textureID_);
}

void
Texture::dispose()
{
	glDeleteTextures(1, &textureID_);
}

// Entity
Entity::Entity() {
	x_ = 0.0f;
	y_ = 0.0f;
	w_ = 0.0f;
	h_ = 0.0f;
}

Entity::Entity(const float& x, const float& y, const float& width, const float& height, const float& rot)
{
	x_ = x;
	y_ = y;
	w_ = width;
	h_ = height;
	rot_ = rot;
}

float
Entity::getX()
{
	return x_;
}

float
Entity::getY()
{
	return y_;
}

float
Entity::getWidth()
{
	return w_;
}

float
Entity::getHeight()
{
	return h_;
}

float
Entity::getRotation() {
	return rot_;
}

void
Entity::setX(const float& val)
{
	x_ = val;
}
void
Entity::setY(const float& val)
{
	y_ = val;
}
void
Entity::setWidth(const float& val)
{
	w_ = val;
}
void
Entity::setHeight(const float& val)
{
	h_ = val;
}

void
Entity::setRotation(const float& val) {
	rot_ = val;
}

void
Entity::move(const float& x, const float& y)
{
	x_ += x;
	y_ += y;
}

void
Entity::rotate(const float& rot)
{
	rot_ += rot;
}

void
Entity::setPosition(const float& x, const float& y)
{
	x_ = x;
	y_ = y;
}

void
Entity::setSize(const float& width, const float& height)
{
	w_ = width;
	h_ = height;
}

bool
Entity::contains(const float& x, const float& y) {
	return (x >= x_ && x <= x_ + w_ && y >= y_ && y <= y_ + h_) ? true : false;
}

bool
Entity::contains(Entity& entity) {
	if (contains(entity.getX(), entity.getY()))
		return true;
	if (contains(entity.getX() + entity.getWidth(), entity.getY()))
		return true;
	if (contains(entity.getX() + entity.getWidth(), entity.getY() + entity.getHeight()))
		return true;
	if (contains(entity.getX(), entity.getY() + entity.getHeight()))
		return true;
	if (entity.contains(x_, y_))
		return true;
	if (entity.contains(x_ + w_, y_))
		return true;
	if (entity.contains(x_ + w_, y_ + h_))
		return true;
	if (entity.contains(x_, y_ + h_))
		return true;
	return false;
}

// Sprite
Sprite::Sprite(const float& x, const float& y, const float& width, const float& height, const float& rot, const GLColor& color) : Entity(x, y, width, height, rot)
{
	usingTexture = false;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	generateVAO(color, TexCoords{0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f});
}

Sprite::Sprite(const float& x, const float& y, const float& width, const float& height, const float& rot, const GLColor& color, Texture* tex,
	const TexCoords& texCoords) : Entity(x, y, width, height, rot)
{
	usingTexture = true;
	texture_ = tex;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	generateVAO(color, texCoords);
}

void
Sprite::generateVAO(const GLColor& color, const TexCoords& texCoords)
{
	GLfloat vertices[] = {
		0.5f,  0.5f, 0.0f, color.r, color.g, color.b, color.a, texCoords.x1, texCoords.y1,
		0.5f, -0.5f, 0.0f, color.r, color.g, color.b, color.a, texCoords.x2, texCoords.y2,
		-0.5f, -0.5f, 0.0f, color.r, color.g, color.b, color.a, texCoords.x3, texCoords.y3,
		-0.5f,  0.5f, 0.0f, color.r, color.g, color.b, color.a, texCoords.x0, texCoords.y0
	};

	GLuint indices[] = {
		0, 1, 3,
		1, 2, 3
	};

	glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)(0));
		glEnableVertexAttribArray(0);

		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);

		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (GLvoid*)(7 * sizeof(GLfloat)));
		glEnableVertexAttribArray(2);
	glBindVertexArray(0);

	generateMatrix();
}

void
Sprite::generateMatrix() {
	matrix_ = glm::mat4();
	matrix_ = glm::translate(matrix_, glm::vec3(((x_ + (w_ / 2)) - (GLfloat)windowHalfWidth) / (GLfloat)windowHalfWidth,
		(((y_ + (h_ / 2)) - (GLfloat)windowHalfHeight) / (GLfloat)windowHalfHeight) * -1, 0.0f));
	matrix_ = glm::scale(matrix_, glm::vec3(w_ / (GLfloat)windowHalfWidth, h_ / (GLfloat)windowHalfHeight, 0.0f));
	matrix_ = glm::rotate(matrix_, glm::radians(rot_), glm::vec3(0.0f, 0.0f, 1.0f));
}

void
Sprite::render()
{
	glPushMatrix();
	glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(matrix_));
	if (usingTexture) texture_->bind();
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	glPopMatrix();
}

void
Sprite::setX(const float& val)
{
	Entity::setX(val);
	generateMatrix();
}
void
Sprite::setY(const float& val)
{
	Entity::setY(val);
	generateMatrix();
}
void
Sprite::setWidth(const float& val)
{
	Entity::setWidth(val);
	generateMatrix();
}
void
Sprite::setHeight(const float& val)
{
	Entity::setHeight(val);
	generateMatrix();
}

void
Sprite::setRotation(const float& val) {
	Entity::setRotation(val);
	generateMatrix();
}

void
Sprite::move(const float& x, const float& y)
{
	Entity::move(x, y);
	generateMatrix();
}

void
Sprite::rotate(const float& rot)
{
	Entity::rotate(rot);
	generateMatrix();
}

void
Sprite::setPosition(const float& x, const float& y)
{
	Entity::setPosition(x, y);
	generateMatrix();
}

void
Sprite::setSize(const float& width, const float& height)
{
	Entity::setSize(width, height);
	generateMatrix();
}

void
Sprite::dispose()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
}