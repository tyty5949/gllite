#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>

#ifndef GLLITE_H
#define GLLITE_H

extern GLFWwindow* window;

void initializeGLFW(const int& width, const int& height, const std::string& name, const bool& fullScreen);
void initializeOpenGL();
bool isMouseButtonDown(const int& button);
double getMouseX();
double getMouseY();
double getScrollX();
double getScrollY();
int getWindowWidth();
int getWindowHeight();

struct TexCoords
{
	GLfloat x0, y0, x1, y1, x2, y2, x3, y3;
};

struct GLColor
{
	GLfloat r, g, b, a;
};

extern const GLColor COLOR_RED;
extern const GLColor COLOR_GREEN;
extern const GLColor COLOR_BLUE;
extern const GLColor COLOR_WHITE;
extern const GLColor COLOR_BLACK;

class Texture
{
private:
	GLuint textureID_;
	unsigned int height_, width_, linearSize_, mipMapCount_, fourCC_;

public:
	Texture::Texture() {}
	Texture(const char* imgPath);
	void bind();
	void dispose();
};

class Entity
{
protected:
	float x_, y_, w_, h_, rot_;
	
public:
	Entity();
	Entity(const float& x, const float& y, const float& width, const float& height, const float& rot);
	float getX();
	float getY();
	float getWidth();
	float getHeight();
	float getRotation();
	void setX(const float& val);
	void setY(const float& val);
	void setWidth(const float& val);
	void setHeight(const float& val);
	void setRotation(const float& val);
	void move(const float& x, const float& y);
	void rotate(const float& rot);
	void setPosition(const float& x, const float& y);
	void setSize(const float& width, const float& height);
	bool contains(const float& x, const float& y);
	bool contains(Entity& entity);
};

class Sprite : public Entity
{
private:
	bool usingTexture;
	Texture* texture_;
	GLuint VAO, VBO, EBO;
	glm::mat4 matrix_;

public:
	Sprite() {};
	Sprite(const float& x, const float& y, const float& width, const float& height, const float& rot, const GLColor& color);
	Sprite(const float& x, const float& y, const float& width, const float& height,const float& rot, const GLColor& color, Texture* tex, const TexCoords& texCoords);
	void generateVAO(const GLColor& color, const TexCoords& textCoords);
	void generateMatrix();
	void render();
	void setX(const float& val);
	void setY(const float& val);
	void setWidth(const float& val);
	void setHeight(const float& val);
	void setRotation(const float& val);
	void move(const float& x, const float& y);
	void rotate(const float& rot);
	void setPosition(const float& x, const float& y);
	void setSize(const float& width, const float& height);
	void dispose();
};

#endif GLLITE_H